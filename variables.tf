variable "location" {
  description = "The default Azure region for the resource provisioning"
  default = "westeurope"
}

variable "rg_name" {
  description = "Resource group name that will contain resources"
  default = "RG-MF-1"
}

variable "subnet_ip_master" {
    description = "Subnet for mgmt servers"
    default = "10.10.1.0/24"
}
variable "subnet_ip_f" {
  description = "IP for Subnet f within a Virtual Network"
  default = "10.10.2.0/24"
}

variable "subnet_ip_b" {
  description = "IP for Subnet b within a Virtual Network"
  default = "10.10.3.0/24"
}

variable "vnet_ip_main" {
  description = "IP for Virtual Network"
  default = "10.10.0.0/16"
}

variable "nsg_front" {
  default = "nsg-front"
}

variable "nsg_back" {
  default = "nsg-back"
}

variable "adminfior" {
  default = "adminfior"
}








