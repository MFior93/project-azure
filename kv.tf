data "azurerm_key_vault" "my-kv" {
  name                = "mf-kv-deploy"
  resource_group_name = "key-vault-deployment"
}

output "keyvault" {
    value = data.azurerm_key_vault.my-kv.id
}

data "azurerm_key_vault_secret" "admin_password" {
  name         = "adminP"
  key_vault_id = data.azurerm_key_vault.my-kv.id
}

output "adminP" {
  value = data.azurerm_key_vault_secret.admin_password.value
}


data "azurerm_key_vault_secret" "admin_login" {
  name         = "adminL"
  key_vault_id = data.azurerm_key_vault.my-kv.id
}

output "adminL" {
  value = data.azurerm_key_vault_secret.admin_login.value
}


data "azurerm_key_vault_secret" "sshpub_master" {
  name         = "sshmaster"
  key_vault_id = data.azurerm_key_vault.my-kv.id
}

output "sshmaster" {
  value = data.azurerm_key_vault_secret.sshpub_master.value
}


data "azurerm_key_vault_secret" "sshpub_adminfior" {
  name         = "sshadminfior"
  key_vault_id = data.azurerm_key_vault.my-kv.id
}

output "sshadminfior" {
  value = data.azurerm_key_vault_secret.sshpub_adminfior.value
}
