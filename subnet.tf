resource "azurerm_subnet" "subnet-f" {
 name                 = "subnet-front"
 resource_group_name  = "${azurerm_resource_group.rg-main.name}"
 virtual_network_name = "${azurerm_virtual_network.vnet-main.name}"
 address_prefix       = "${var.subnet_ip_f}"
}

resource "azurerm_subnet" "subnet-b" {
 name                 = "subnet-back"
 resource_group_name  = "${azurerm_resource_group.rg-main.name}"
 virtual_network_name = "${azurerm_virtual_network.vnet-main.name}"
 address_prefix       = "${var.subnet_ip_b}"
}

resource "azurerm_subnet" "subnet-master" {
 name = "subnet-master"
 address_prefix = "${var.subnet_ip_master}"
 virtual_network_name = "${azurerm_virtual_network.vnet-main.name}"
 resource_group_name = "${azurerm_resource_group.rg-main.name}"
}