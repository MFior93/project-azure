resource "azurerm_virtual_machine" "vm-master" {
  name                  = "vm-master"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.rg-main.name}"
  network_interface_ids = ["${azurerm_network_interface.nic-master.id}"]
  vm_size               = "Standard_DS1_v2"

#This will delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true


  storage_image_reference {
    id = "/subscriptions/d5c73fb6-3e1a-4942-92d5-6acf6d976b93/resourceGroups/gitlab-runner/providers/Microsoft.Compute/images/master-image"
  }

  storage_os_disk {
    name              = "master-os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "vm-master"
    admin_username = "${var.adminfior}"
    admin_password = "${data.azurerm_key_vault_secret.admin_password.value}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
    ssh_keys {
      path     = "/home/${var.adminfior}/.ssh/authorized_keys"
      key_data = "${data.azurerm_key_vault_secret.sshpub_master.value}"
    }
  }
}