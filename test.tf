data "azurerm_image" "main-master-img" {
  name                = "master-image"
  resource_group_name = "gitlab-runner"
}

output "image_id" {
  value = data.azurerm_image.main-master-img.id
}