resource "azurerm_network_interface" "nic-front" {
 count               = 2
 name                = "nic-front${count.index}"
 location            = "${var.location}"
 resource_group_name = "${azurerm_resource_group.rg-main.name}"

 ip_configuration {
   name                          = "nic-ipconf-f"
   subnet_id                     = "${azurerm_subnet.subnet-f.id}"
   private_ip_address_allocation = "dynamic"
   public_ip_address_id          = "${element(azurerm_public_ip.pub-ip-f.*.id, count.index + 1 )}"
 }
}

resource "azurerm_network_interface_backend_address_pool_association" "nic-lb-pool-f" {
  count                   = 2
  network_interface_id    = "${element(azurerm_network_interface.nic-front.*.id, count.index + 1 )}"
  ip_configuration_name   = "nic-ipconf-f"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.lb-addrpool-f.id}"
}

resource "azurerm_network_interface_security_group_association" "nic-nsg-front" {
  count                     = 2
  network_interface_id      = "${element(azurerm_network_interface.nic-front.*.id, count.index + 1 )}"
  network_security_group_id = "${azurerm_network_security_group.nsg-front.id}"
}

resource "azurerm_network_interface" "nic-back" {
 count               = 2
 name                = "nic-back${count.index}"
 location            = "${var.location}"
 resource_group_name = "${azurerm_resource_group.rg-main.name}"

 ip_configuration {
   name                          = "nic-ipconf-b"
   subnet_id                     = "${azurerm_subnet.subnet-b.id}"
   private_ip_address_allocation = "dynamic"
   public_ip_address_id          = "${element(azurerm_public_ip.pub-ip-b.*.id, count.index + 1 )}"
 }
}
resource "azurerm_network_interface_backend_address_pool_association" "nic-lb-pool-b" {
  count                   = 2
  network_interface_id    = "${element(azurerm_network_interface.nic-back.*.id, count.index + 1 )}"
  ip_configuration_name   = "nic-ipconf-b"
  backend_address_pool_id = "${azurerm_lb_backend_address_pool.lb-addrpool-b.id}"
}

resource "azurerm_network_interface_security_group_association" "nic-nsg-back" {
  count                     = 2
  network_interface_id      = "${element(azurerm_network_interface.nic-back.*.id, count.index + 1)}"
  network_security_group_id = "${azurerm_network_security_group.nsg-back.id}"
}

resource "azurerm_network_interface" "nic-master" {
    name                      = "nic-master"
    location                  = "${var.location}"
    resource_group_name       = "${azurerm_resource_group.rg-main.name}"

    ip_configuration {
        name                          = "nic-ipconf-master"
        subnet_id                     = "${azurerm_subnet.subnet-master.id}"
        private_ip_address_allocation = "Static"
        private_ip_address            = "10.10.1.4"
        public_ip_address_id          = "${azurerm_public_ip.pip-master.id}"
    }
}

resource "azurerm_network_interface_security_group_association" "nic-nsg-master" {
  network_interface_id      = "${azurerm_network_interface.nic-master.id}"
  network_security_group_id = "${azurerm_network_security_group.nsg-back.id}"
}