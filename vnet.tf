resource "azurerm_virtual_network" "vnet-main" {
 name                = "vnet-m"
 address_space       = ["${var.vnet_ip_main}"]
 location            = "${var.location}"
 resource_group_name = "${azurerm_resource_group.rg-main.name}"
}