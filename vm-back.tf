resource "azurerm_virtual_machine" "vm-back" {
 count                 = 2
 name                  = "back_vm_${count.index}"
 location              = "${var.location}"
 availability_set_id   = "${azurerm_availability_set.availabilityset-back.id}"
 resource_group_name   = "${azurerm_resource_group.rg-main.name}"
 network_interface_ids = [element(azurerm_network_interface.nic-back.*.id, count.index)]
 vm_size               = "Standard_DS1_v2"

 # Uncomment this line to delete the OS disk automatically when deleting the VM
 # delete_os_disk_on_termination = true

 # Uncomment this line to delete the data disks automatically when deleting the VM
 # delete_data_disks_on_termination = true

 storage_image_reference {
   publisher = "Canonical"
   offer     = "UbuntuServer"
   sku       = "16.04-LTS"
   version   = "latest"
 }

 storage_os_disk {
   name              = "myosdisk-b${count.index}"
   caching           = "ReadWrite"
   create_option     = "FromImage"
   managed_disk_type = "Standard_LRS"
 }

 os_profile {
   computer_name  = "back"
   admin_username = "${var.adminfior}"
   admin_password = "${data.azurerm_key_vault_secret.admin_password.value}"
 }

  os_profile_linux_config {
    disable_password_authentication = false
    ssh_keys {
      path     = "/home/${var.adminfior}/.ssh/authorized_keys"
      key_data = "${data.azurerm_key_vault_secret.sshpub_adminfior.value}"
    }
  }
}