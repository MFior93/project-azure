resource "azurerm_public_ip" "pip-lb-front" {
 name                         = "pip-lb-front"
 location                     = "${var.location}"
 resource_group_name          = "${azurerm_resource_group.rg-main.name}"
 allocation_method            = "Static"
}

resource "azurerm_public_ip" "pip-lb-back" {
 name                         = "pip-lb-back"
 location                     = "${var.location}"
 resource_group_name          = "${azurerm_resource_group.rg-main.name}"
 allocation_method            = "Static"
}

resource "azurerm_public_ip" "pub-ip-f" {
  count                 = 2
  name                  = "pub_ip_f_${count.index}"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.rg-main.name}"
  allocation_method     = "Static"
}

resource "azurerm_public_ip" "pub-ip-b" {
  count                 = 2
  name                  = "pub_ip_b_${count.index}"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.rg-main.name}"
  allocation_method     = "Static"
}

resource "azurerm_public_ip" "pip-master" {
  name                          = "pip-master"
  location                      = "${var.location}"
  resource_group_name           = "${azurerm_resource_group.rg-main.name}"
  allocation_method             = "Static"
}